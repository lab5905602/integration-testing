import express from 'express'
import routes from './routes.js'

const app = express();

// Enable CORS for all routes
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:YOUR_PORT');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use('/api/v1', routes);

const api_url = "http://localhost:3000/api/v1"
app.listen(3000, () => console.log(
    `Server listening at ${api_url}`
));

