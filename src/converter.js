
/**
 * Converts rgb color to hex
 * @param {number} r red value 
 * @param {number} g green value
 * @param {number} b blue value
 * @returns {string} hex color string
 */
export const rgb_to_hex = (r, g, b) => {
    let ans = "#" + toHex(r) + toHex(g) + toHex(b)
    return ans
}

/**
 * Converts a decimal value to hexadecimal and
 * adds leading zero if necessary.
 * @param {number} num decimal value
 * @returns {string} hexadecimal value as string
 */
const toHex = (num) => {
    let hex = num.toString(16)
    if (num < 16) { hex = "0" + hex }
    return hex
}

/**
 * Convert hexadecimal string back to 
 * rgb-values. String can have hashtag or
 * it can be missing.
 * @param {string} hex hex-string
 * @returns {Array<Int>} RGB-values
 */
export const hex_to_rgb = (hex) => {
    if (hex.length == 7) { hex = hex.substring(1, 7) }
    const H2D = h => parseInt(h, 16);
    const ans = [
        H2D(hex.substring(0, 2)),
        H2D(hex.substring(2, 4)),
        H2D(hex.substring(4, 6)),
    ]
    return ans
}

