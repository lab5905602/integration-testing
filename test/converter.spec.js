import { describe, it } from "mocha";
import { expect } from "chai";
import { hex_to_rgb, rgb_to_hex } from "../src/converter.js";

describe("RGB-to-HEX Converter", () => {
    it("is a function", () => {
        expect(rgb_to_hex).to.be.an('function')
    });
    it("should return a string", () => {
        expect(rgb_to_hex(0, 0, 0)).to.be.a('string')
    });
    it("first character is a hashtag", () => {
        expect(rgb_to_hex(0, 0, 0)[0]).to.equal("#")
    });
    it("should convert RED value correctly", () => {
        expect(rgb_to_hex(  0,   0,   0).substring(0, 3)).to.equal("#00");
        expect(rgb_to_hex(255,   0,   0).substring(0, 3)).to.equal("#ff");
        expect(rgb_to_hex(136,   0,   0).substring(0, 3)).to.equal("#88");
        expect(rgb_to_hex(999,   0,   0).substring(0, 3)).to.not.equal("#12");
    });
    it("should convert GREEN value correctly", () => {
        expect(rgb_to_hex(  0,   0,   0).substring(3, 5)).to.equal("00");
        expect(rgb_to_hex(  0, 255,   0).substring(3, 5)).to.equal("ff");
        expect(rgb_to_hex(  0, 136,   0).substring(3, 5)).to.equal("88");
        expect(rgb_to_hex(  0, 999,   0).substring(3, 5)).to.not.equal("12");
    });
    it("should convert BLUE value correctly", () => {
        expect(rgb_to_hex(  0,   0,   0).substring(5, 7)).to.equal("00");
        expect(rgb_to_hex(  0,   0, 255).substring(5, 7)).to.equal("ff");
        expect(rgb_to_hex(  0,   0, 136).substring(5, 7)).to.equal("88");
        expect(rgb_to_hex(  0,   0, 999).substring(5, 7)).to.not.equal("12");
    });
    it("should convert RGB-to-HEX correctly", () => {
        expect(rgb_to_hex(255,   0,   0)).to.equal("#ff0000");
        expect(rgb_to_hex(  0, 255,   0)).to.equal("#00ff00");
        expect(rgb_to_hex(  0,   0, 255)).to.equal("#0000ff");
        expect(rgb_to_hex(255, 136,   0)).to.equal("#ff8800");
    });
    
});

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.a('function')
    });
    it("should return an array of length 3", () => {
        expect(hex_to_rgb("#ffffff")).to.be.an('Array');
        expect(hex_to_rgb("#ffffff").length).to.equal(3);
    });
    it("should convert RED value correctly", () => {
        expect(hex_to_rgb("#000000")).to.deep.equal([  0, 0, 0]);
        expect(hex_to_rgb("#ff0000")).to.deep.equal([255, 0, 0]);
        expect(hex_to_rgb("#880000")).to.deep.equal([136, 0, 0]);
        expect(hex_to_rgb("#d40000")).to.not.deep.equal([999, 0, 0]);
    });
    it("should convert GREEN value correctly", () => {
        expect(hex_to_rgb("#000000")).to.deep.equal([0,   0, 0]);
        expect(hex_to_rgb("#00ff00")).to.deep.equal([0, 255, 0]);
        expect(hex_to_rgb("#008800")).to.deep.equal([0, 136, 0]);
        expect(hex_to_rgb("#00d400")).to.not.deep.equal([0, 999, 0]);
    });
    it("should convert BLUE value correctly", () => {
        expect(hex_to_rgb("#000000")).to.deep.equal([0, 0,   0]);
        expect(hex_to_rgb("#0000ff")).to.deep.equal([0, 0, 255]);
        expect(hex_to_rgb("#000088")).to.deep.equal([0, 0, 136]);
        expect(hex_to_rgb("#0000d4")).to.not.deep.equal([0, 0, 999]);
    });
    it("should convert HEX-to-RGB correctly", () => {
        expect(hex_to_rgb("#ff0000")).to.deep.equal([255,   0,   0]);
        expect(hex_to_rgb("#00ff00")).to.deep.equal([  0, 255,   0]);
        expect(hex_to_rgb("#0000ff")).to.deep.equal([  0,   0, 255]);
        expect(hex_to_rgb("#ff8800")).to.deep.equal([255, 136,   0]);
    });
    
});
